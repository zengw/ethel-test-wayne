import numpy as np
import ast
import pandas as pd  # Import pandas for handling data structures and CSV output

def read_keyword_lists(file_path):
    with open(file_path, 'r') as file:
        lines = file.readlines()
    keyword_lists = [ast.literal_eval(line.strip()) for line in lines]
    return keyword_lists

def filter_keywords(keyword_lists, min_occurrence=3):
    from collections import Counter
    keyword_occurrences = Counter(keyword for sublist in keyword_lists for keyword in sublist)
    filtered_keywords = {keyword for keyword, count in keyword_occurrences.items() if count >= min_occurrence}
    return filtered_keywords

def construct_relatedness_matrix(keyword_lists, filtered_keywords):
    keyword_index = {keyword: i for i, keyword in enumerate(filtered_keywords)}
    matrix_size = len(filtered_keywords)
    relatedness_matrix = np.zeros((matrix_size, matrix_size))

    for keyword_list in keyword_lists:
        filtered_list = [keyword for keyword in keyword_list if keyword in filtered_keywords]
        for i in range(len(filtered_list)):
            for j in range(i, len(filtered_list)):
                index_i = keyword_index[filtered_list[i]]
                index_j = keyword_index[filtered_list[j]]
                relatedness_matrix[index_i][index_j] += 1
                relatedness_matrix[index_j][index_i] += 1

    return relatedness_matrix, list(filtered_keywords)

def output_relatedness_matrix_to_csv(matrix, keywords, output_file):
    """
    Outputs the relatedness matrix to a CSV file with keywords as headers and index.
    """
    df = pd.DataFrame(matrix, index=keywords, columns=keywords)
    df.to_csv(output_file)
    print(f"Matrix successfully written to {output_file}")

def main(file_path, output_file='relatedness_matrix.csv'):
    keyword_lists = read_keyword_lists(file_path)
    filtered_keywords = filter_keywords(keyword_lists)
    relatedness_matrix, filtered_keyword_list = construct_relatedness_matrix(keyword_lists, filtered_keywords)
    
    # Output the matrix to a CSV file
    output_relatedness_matrix_to_csv(relatedness_matrix, filtered_keyword_list, output_file)

# Example usage
file_path = './db_dumps/mathematik_keywords.txt'  # Update this with the actual path to your file
main(file_path)

