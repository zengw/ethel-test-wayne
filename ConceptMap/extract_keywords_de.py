import nltk
nltk.download('stopwords')
from nltk.corpus import stopwords
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction import text
import chromadb

my_stop_words = stopwords.words('german')
nowords = ['cid']

client = chromadb.PersistentClient(path="./Physik/chroma_db")

coll = client.get_collection("langchain")
data=coll.get()

documents = data["documents"]

for doc in documents:
#   cleaned_text = ''.join([char if ord(char) < 128 else ' ' for char in doc])
   cleaned_text = doc
# Using TF-IDF to extract keywords
   vectorizer = TfidfVectorizer(stop_words=my_stop_words, max_features=10)
   X = vectorizer.fit_transform([cleaned_text])
   features = vectorizer.get_feature_names_out()
   filtered_features = [feature for feature in features if not feature.isdigit() and feature not in nowords]
   print(filtered_features)
