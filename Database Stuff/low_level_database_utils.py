# Project Ethel
#
# Low-level routines to directly handle the databases
# !!! No permission checking at this level !!!
# Always go through higher-level functions
# !!! NO SQL BEYOND THIS POINT !!!
#
# Copyright (C) 2024 Gerd Kortemeyer, ETH Zurich
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
import logging
from postgres_connection_pool import get_db_cursor
from psycopg2 import DatabaseError, IntegrityError, OperationalError
import json

#
# Function to execute single SQL commands
#
def execute_db_command(sql, params=None):
    try:
        with get_db_cursor(commit=True) as cur:
            cur.execute(sql, params or ())
            if sql.strip().upper().startswith("SELECT") or "RETURNING" in sql.upper():
                results = cur.fetchall()  # Retrieve all rows from SELECT
                return True, results
            else:
                return True, "Command executed successfully"  # For UPDATE, DELETE where no return is expected
    except IntegrityError as e:
        return False, f"Failed to insert data due to integrity constraints: {e}"
    except OperationalError as e:
        return False, f"Failed to connect or execute command: {e}"
    except DatabaseError as e:
        return False, f"General database error: {e}"
    except Exception as e:
        return False, f"An unexpected error occurred: {e}"


# ==============================================================================
# Functions having to do with authentication types
# Authentication types would be things like "Switch EduID" or "Active Directory"
#

# This make a new authentication type, probably does not happen often
#
def add_new_authentication_type(auth_type_name):
    """
    Adds a new authentication type if it does not already exist.
    """
    check_query = "SELECT AuthTypeID FROM AuthenticationTypes WHERE AuthTypeName = %s;"
    success, result = execute_db_command(check_query, (auth_type_name,))
    if success and result:
        logging.warning(f"Authentication type '{auth_type_name}' already exists.")
        return False, "Authentication type already exists."

    insert_query = "INSERT INTO AuthenticationTypes (AuthTypeName) VALUES (%s) RETURNING AuthTypeID;"
    success, result = execute_db_command(insert_query, (auth_type_name,))
    if success:
        logging.info(f"New authentication type added with ID: {result[0]}")
        return True, result[0]
    else:
        logging.error(f"Error adding authentication type: {result}")
        return False, result

# Renaming an authentication type, probably does not happen often
#
def rename_authentication_type(old_name, new_name):
    """
    Renames an authentication type if the new name is not already in use.
    """
    check_query = "SELECT AuthTypeID FROM AuthenticationTypes WHERE AuthTypeName = %s;"
    success, result = execute_db_command(check_query, (new_name,))
    if success and result:
        logging.warning(f"Authentication type '{new_name}' already exists.")
        return False, "Authentication type already exists."

    update_query = "UPDATE AuthenticationTypes SET AuthTypeName = %s WHERE AuthTypeName = %s RETURNING AuthTypeID;"
    success, result = execute_db_command(update_query, (new_name, old_name))
    if success:
        if result:
            logging.info(f"Authentication type '{old_name}' renamed to '{new_name}' for {result[0]}.")
            return True, result[0]
        else:
            logging.warning(f"No authentication type found with the name '{old_name}'.")
            return False, None
    else:
        logging.error(f"Error renaming authentication type: {result}")
        return False, result

# This gets the user_id behind a username in an authentication type
#
def get_userid_from_auth(auth_type_name, auth_user_id):
    """
    Retrieves the UserID based on the given AuthTypeName and AuthUserID.
    """
    # Get AuthTypeID from AuthTypeName
    get_auth_type_id_query = "SELECT AuthTypeID FROM AuthenticationTypes WHERE AuthTypeName = %s;"
    success, result = execute_db_command(get_auth_type_id_query, (auth_type_name,))
    if success and result:
        auth_type_id = result[0]
    else:
        logging.error(f"Error fetching AuthTypeID for AuthTypeName '{auth_type_name}': {result}")
        return False, f"Error fetching AuthTypeID for AuthTypeName '{auth_type_name}': {result}"

    # Get UserID from AuthTypeID and AuthUserID
    get_user_id_query = """
        SELECT UserID FROM AuthenticationUserIDs 
        WHERE AuthTypeID = %s AND AuthUserID = %s;
    """
    success, result = execute_db_command(get_user_id_query, (auth_type_id, auth_user_id))
    if success:
        if result:
            logging.info(f"UserID for AuthTypeName '{auth_type_name}' and AuthUserID '{auth_user_id}' is '{result[0]}'.")
            return True, result[0]
        else:
            logging.warning(f"No UserID found for AuthTypeName '{auth_type_name}' and AuthUserID '{auth_user_id}'.")
            return False, None
    else:
        logging.error(f"Error fetching UserID for AuthTypeName '{auth_type_name}' and AuthUserID '{auth_user_id}': {result}")
        return False, result

# ==============================================================================
# Functions having to do with users
#

# This makes a new user, which is a pretty big deal
#
def add_new_user(auth_internal_user_id, auth_type_name, first_name, last_name):
    """
    Adds a new user with the specified details, ensuring all database operations
    are either all committed or all rolled back if an error occurs. This includes creating
    a new resource and linking it to the user's TopLevelResourceID.
    """
    try:
        with get_db_cursor() as cur:
            # Check if AuthUserID already exists for AuthTypeName
            check_query = """
            SELECT a.AuthUserID FROM AuthenticationUserIDs a
            JOIN AuthenticationTypes t ON a.AuthTypeID = t.AuthTypeID
            WHERE a.AuthUserID = %s AND t.AuthTypeName = %s;
            """
            cur.execute(check_query, (auth_internal_user_id, auth_type_name))
            if cur.fetchone():
                logging.warning(f"User {auth_internal_user_id} with authentication type {auth_type_name} already exists.")
                return False, "User with given authentication details already exists."

            # Insert new user
            user_insert = """
            INSERT INTO Users (FirstName, LastName) VALUES (%s, %s) RETURNING UserID;
            """
            cur.execute(user_insert, (first_name, last_name))
            new_user_id = cur.fetchone()[0]  # Fetch the UUID of the newly created user

            # Insert new resource linked to this user
            resource_insert = """
            INSERT INTO Resources (ResourceName, ResourceType) VALUES (%s, 'user_root') RETURNING ResourceID;
            """
            cur.execute(resource_insert, (new_user_id,))
            new_resource_id = cur.fetchone()[0]

            # Update the new user's TopLevelResourceID with the newly created resource ID
            update_user = """
            UPDATE Users SET TopLevelResourceID = %s WHERE UserID = %s;
            """
            cur.execute(update_user, (new_resource_id, new_user_id))

            # Retrieve the AuthTypeID
            get_auth_type_id_query = """
            SELECT AuthTypeID FROM AuthenticationTypes WHERE AuthTypeName = %s;
            """
            cur.execute(get_auth_type_id_query, (auth_type_name,))
            auth_type_id = cur.fetchone()
            if not auth_type_id:
                raise Exception("Authentication type does not exist")

            # Insert new AuthenticationUserID entry
            auth_user_insert = """
            INSERT INTO AuthenticationUserIDs (AuthUserID, AuthTypeID, UserID)
            VALUES (%s, %s, %s);
            """
            cur.execute(auth_user_insert, (auth_internal_user_id, auth_type_id[0], new_user_id))
            cur.connection.commit()  # Commit all changes if all operations succeed
            logging.info(f"Created user {auth_internal_user_id} with authentication method {auth_type_name} with root resource ID {new_resource_id}: {new_user_id}")
            return True, new_user_id
    except Exception as e:
        cur.connection.rollback()  # Rollback the transaction in case of any failure
        logging.error(f"User {auth_internal_user_id} with authentication method {auth_type_name} not created: {e}")
        return False, f"An error occurred: {e}"


# Example usage:
# update_result = update_user(user_uuid, FirstName="Alice", LastName="Smith")
# print(update_result)
#
def update_user(user_id, **kwargs):
    """
    Update user record in the Users table.
    Args:
    user_id (UUID): The UUID of the user to update.
    **kwargs: Arbitrary keyword arguments representing column names and their new values.

    Returns:
    (bool, str): Tuple of success flag and either result or error message.
    """
    valid_columns = {
        "FirstName", "FirstNameTranscription", "MiddleNames", "MiddleNamesTranscription",
        "LastName", "LastNameTranscription", "GenerationQualifier", "GenerationQualifierTranscription",
        "TopLevelResourceID"
    }
    columns_to_update = {key: kwargs[key] for key in kwargs if key in valid_columns}

    if not columns_to_update:
        return False, "No valid or existing columns provided for update."

    set_clauses = ", ".join([f"{key} = %s" for key in columns_to_update])
    values = list(columns_to_update.values())

    sql = f"UPDATE Users SET {set_clauses} WHERE UserID = %s;"
    values.append(user_id)

    # Now we pass it to your existing database command execution function
    success, result_or_error = execute_db_command(sql, values)
    if success:
        logging.info(f"Successfully updated user {user_id}. Changes: {kwargs}")
        return True, "Update successful."
    else:
        logging.warning(f"Failed to update user {user_id}. Error: {result_or_error}")
        return False, result_or_error

# Fuzzy search
# Performs an AND between all **kwargs
# 
def search_user_by_field(max_output, **kwargs):
    """
    Conducts a fuzzy search on user data based on multiple specified fields using PostgreSQL's similarity function.
    It creates a dynamic SQL query that performs an AND operation between all provided keyword arguments that are valid user fields.
    This function also supports searching transcription fields and joins with related tables (authentication and community) as needed.

    Args:
        max_output (int): Maximum number of results to return.
        **kwargs: Arbitrary keyword arguments representing fields to be searched. The valid fields include:
            - FirstName, MiddleNames, LastName, GenerationQualifier (with their respective transcriptions)
            - AuthUserID, AuthTypeName (from authentication-related tables)
            - CommunityUserID, CommunityName (from community-related tables)

    Returns:
        tuple: (success (bool), result_or_error (mixed))
            - On success: Returns (True, list of results) where results could be an empty list if no matches are found.
            - On failure: Returns (False, error message) describing why the search failed.

    Example:
        search_user_by_field(10, FirstName="John", LastName="Doe")
        This would search for users with first name and last name similar to "John" and "Doe" respectively.

    Note:
        The similarity threshold is hard-coded at 0.1 in this implementation. Adjustments to this value should be made based on specific accuracy needs.
    """
    valid_columns = {
        "FirstName", "MiddleNames", "LastName", "GenerationQualifier",
        "AuthUserID", "AuthTypeName", "CommunityUserID", "CommunityName"
    }
    # Filter and retain only valid columns
    search_params = {key: kwargs[key] for key in kwargs if key in valid_columns and kwargs[key] is not None}

    if not search_params:
        return False, "No valid or existing columns provided for search."

    # Prepare the SQL query components
    select_clauses = ["Users.*"]
    from_clauses = ["Users"]
    where_clauses = []
    values = []
    similarity_clauses = []
    # Make sure we only join the authentication and community tables if needed, but also only once
    added_auth_join = False
    added_community_join = False

    # Handle dual matching for transcribed fields and regular fields
    transcribed_fields = {
        "FirstName": "FirstNameTranscription",
        "MiddleNames": "MiddleNamesTranscription",
        "LastName": "LastNameTranscription",
        "GenerationQualifier": "GenerationQualifierTranscription"
    }

    # Building the dynamic query
    for field, value in search_params.items():
        if field in transcribed_fields:
            # Fields for which transcriptions exist
            transcription_field = transcribed_fields[field]
            where_clauses.append(f"(similarity(Users.{field}, %s) > 0.1 OR similarity(Users.{transcription_field}, %s) > 0.1)")
            similarity_clauses.append(f"(COALESCE(similarity(Users.{field}, %s),0) + COALESCE(similarity(Users.{transcription_field}, %s),0))")
            values.extend([value, value]) 
        elif field in ["AuthUserID", "AuthTypeName"]:
            if not added_auth_join:
                from_clauses.extend([
                    "LEFT JOIN AuthenticationUserIDs ON Users.UserID = AuthenticationUserIDs.UserID",
                    "LEFT JOIN AuthenticationTypes ON AuthenticationUserIDs.AuthTypeID = AuthenticationTypes.AuthTypeID"
                ])
                added_auth_join = True
            where_clauses.append("similarity(AuthenticationTypes.AuthTypeName, %s) > 0.1" if field == "AuthTypeName" else "similarity(AuthenticationUserIDs.AuthUserID, %s) > 0.1")
            similarity_clauses.append("similarity(AuthenticationTypes.AuthTypeName, %s)" if field == "AuthTypeName" else "similarity(AuthenticationUserIDs.AuthUserID, %s)")
            values.append(value)
        elif field in ["CommunityUserID", "CommunityName"]:
            if not added_community_join:
                from_clauses.extend([
                    "LEFT JOIN CommunityUserIDs ON Users.UserID = CommunityUserIDs.UserID",
                    "LEFT JOIN Communities ON CommunityUserIDs.CommunityID = Communities.CommunityID"
                ])
                added_community_join = True
            where_clauses.append("similarity(Communities.CommunityName, %s) > 0.1" if field == "CommunityName" else "similarity(CommunityUserIDs.CommunityUserID, %s) > 0.1")
            similarity_clauses.append("similarity(Communities.CommunityName, %s)" if field == "CommunityName" else "similarity(CommunityUserIDs.CommunityUserID, %s)")
            values.append(value)
        else:
            where_clauses.append(f"similarity(Users.{field}, %s) > 0.1")
            similarity_clauses.append(f"similarity(Users.{field}, %s)")
            values.append(value)


    # Join all WHERE, FROM clauses
    where_statement = ' AND '.join(where_clauses)
    from_statement = ' '.join(from_clauses)

    # Prepare the SQL query with sorting and limit
    similarity_statement = ' + '.join(similarity_clauses) if similarity_clauses else '1'  # Default to 1 if no similarity
    sql = f"SELECT {', '.join(select_clauses)}, ({similarity_statement}) AS relevance FROM {from_statement} WHERE {where_statement} ORDER BY relevance DESC, Users.LastName LIMIT %s;"

    # Doubling the values for each %s in the SQL
    doubled_values = values * 2
    doubled_values.append(max_output)

    logging.info(f"SQL: {sql}")
    logging.info(f"Val: {doubled_values}")

    # Execute the query using the shared database command function
    success, result_or_error = execute_db_command(sql, doubled_values)
    if success:
        return True, result_or_error if result_or_error else []
    else:
        return False, result_or_error

# ==============================================================================
# Functions having to do with communities
# Communities would be things like particular offices on campus, courses, course sections, ...
#

# Make a new community, which could be a subcommunity of parentcommunityid
#
def add_new_community(name, parentcommunityid=None):
    """
    Adds a new community with the specified details, ensuring all database operations
    are either all committed or all rolled back if an error occurs. This includes creating
    a new root resource for the community, named after the community's UUID.
    Parameters:
    - name (str): The name of the community.
    - parentcommunityid (UUID|None): The optional UUID of the parent community.
    """
    try:
        with get_db_cursor() as cur:
            # Insert the new community
            community_insert = """
            INSERT INTO Communities (CommunityName, ParentCommunityID)
            VALUES (%s, %s) RETURNING CommunityID;
            """
            cur.execute(community_insert, (name, parentcommunityid))
            new_community_id = cur.fetchone()[0]  # Fetch the UUID of the newly created community

            # Create a new root resource for the community using the new community's UUID as the resource name
            resource_insert = """
            INSERT INTO Resources (ResourceName, ResourceType) VALUES (%s, 'community_root') RETURNING ResourceID;
            """
            cur.execute(resource_insert, (str(new_community_id),))
            new_resource_id = cur.fetchone()[0]

            # Update the new community's TopLevelResourceID with the newly created resource ID
            update_community = """
            UPDATE Communities SET TopLevelResourceID = %s WHERE CommunityID = %s;
            """
            cur.execute(update_community, (new_resource_id, new_community_id))

            cur.connection.commit()  # Commit all changes if all operations succeed
            logging.info(f"Created new community '{name}' with ID: {new_community_id} and root resource ID: {new_resource_id}")
            return True, new_community_id
    except Exception as e:
        cur.connection.rollback()  # Rollback the transaction in case of any failure
        logging.error(f"Failed to create community '{name}': {e}")
        return False, f"An error occurred: {e}"

# Retrace the communities and subcommunities, for example exercise groups within sections within courses
#
def get_community_relationship_trail(community_id):
    """
    Retrieves a list of parent community IDs starting from the given community
    up to the top-level ancestor, and returns the TopLevelResourceID of the ultimate ancestor.

    Args:
        community_id (UUID): The starting community ID.

    Returns:
        tuple: (success (bool), message (str or list), top_level_resource_id (UUID or None))
    """
    recursive_query = """
    WITH RECURSIVE ParentChain AS (
        SELECT CommunityID, ParentCommunityID, TopLevelResourceID
        FROM Communities
        WHERE CommunityID = %s
    UNION ALL
        SELECT c.CommunityID, c.ParentCommunityID, c.TopLevelResourceID
        FROM Communities c
        JOIN ParentChain pc ON c.CommunityID = pc.ParentCommunityID
    )
    SELECT CommunityID, TopLevelResourceID FROM ParentChain;
    """
    try:
        with get_db_cursor() as cur:
            cur.execute(recursive_query, (community_id,))
            results = cur.fetchall()
            
            if not results:
                return False, "No community found with the given ID", None

            community_ids = [result[0] for result in results]
            top_level_resource_id = results[-1][1]  # Assume the last one is the top, as it has no parent
            
            return True, community_ids, top_level_resource_id
    except Exception as e:
        logging.error(f"An error occurred while fetching parent communities: {e}")
        return False, f"An error occurred: {e}", None

# ==============================================================================
# Functions having to do with resources
#

# Make a new resource of resource_type
#
def add_new_resource(resource_name, resource_type):
    """
    Adds a new resource
    """
    insert_query = "INSERT INTO Resources (ResourceName, ResourceType) VALUES (%s, %s) RETURNING ResourceID;"
    success, result = execute_db_command(insert_query, (resource_name, resource_type))
    if success:
        logging.info(f"New resource added with ID: {result[0]}")
        return True, result[0]
    else:
        logging.error(f"Error adding resource: {result}")
        return False, result

# Make a new folder resource
#
def add_new_folder(folder_name):
    """
    Adds a new folder, which is a specific type of resource.
    """
    return add_new_resource(folder_name, 'folder')

# List all active resources in a folder
#
def list_folder_contents(resource_id):
    """
    Lists the contents of a folder identified by resource_id.
    Outputs all rows (child resources) associated with the given resourceID.
    """
    query = """
        SELECT rr.ChildResourceID, r.ResourceName, r.ResourceType, rr.DisplayName, rr.ChildOrder, rr.IsAssessment
        FROM ResourceRelationships rr
        JOIN Resources r ON rr.ChildResourceID = r.ResourceID
        WHERE rr.ParentResourceID = %s AND rr.ChildOrder IS NOT NULL
        ORDER BY rr.ChildOrder;
    """
    try:
        success, results = execute_db_command(query, (resource_id,))
        if success:
            if results:
                return True, results
            else:
                return True, "No contents found."
        else:
            return False, "Failed to retrieve folder contents."
    except Exception as e:
        return False, f"An error occurred while listing folder contents: {e}"

# Get the resource tree, starting at a resource_id
#
def list_folder_tree(resource_id):
    """
    Generates a JSON structure representing the resource tree starting from the given relationship ID.

    Args:
        relationship_id (UUID): The ID of the starting relationship.

    Returns:
        tuple: (success (bool), tree (dict or str))
    """
    def fetch_children(resource_id):
        """
        Recursively fetches children of a given resource to build the tree.

        Args:
            resource_id (UUID): The ID of the parent resource to fetch children for.

        Returns:
            list: A list of child nodes in the tree.
        """
        success, results = list_folder_contents(resource_id)
        if not success:
            return False, results  # Propagate the error message up the call stack

        if isinstance(results, str):  # Handle the "No contents found." message
            return [], None  # Return an empty list for this branch

        nodes = []
        for child in results:
            node = {
                'id': child[0],  # RelationshipID as ID
                'name': child[3]  # DisplayName of the child
            }
            if child[2] == 'folder':  # Check if the resource type is 'folder'
                child_success, child_nodes = fetch_children(child[0])  # Recursive call
                if not child_success:
                    return False, child_nodes  # Propagate the error message up if any
                node['children'] = child_nodes
            nodes.append(node)
        return True, nodes

    root_success, root_nodes = fetch_children(resource_id)
    if not root_success:
        return False, "Failed to build tree."

    return True, root_nodes

# List all inactive resource instances in a folder, in case you want to restore them
#
def list_inactive_folder_contents(resource_id):
    """
    Lists the inactive contents of a folder identified by resource_id.
    Inactive contents are those where ChildOrder is NULL, indicating they are not actively ordered within the folder.
    """
    query = """
        SELECT rr.ChildResourceID, r.ResourceName, r.ResourceType, rr.DisplayName
        FROM ResourceRelationships rr
        JOIN Resources r ON rr.ChildResourceID = r.ResourceID
        WHERE rr.ParentResourceID = %s AND rr.ChildOrder IS NULL
        ORDER BY r.ResourceName; 
    """
    try:
        success, results = execute_db_command(query, (resource_id,))
        if success:
            if results:
                return True, results
            else:
                return True, "No inactive contents found."
        else:
            return False, "Failed to retrieve inactive folder contents."
    except Exception as e:
        return False, f"An error occurred while listing inactive folder contents: {e}"

# List all the leaves of the tree, top down
#
def list_tree_leaves(resource_id):
    """
    Generates an array of all leaf nodes (non-folder resources) from the tree starting from the given resource ID.
    Each leaf node contains the RelationshipID and IsAssessment attribute.

    Args:
        resource_id (UUID): The ID of the starting resource.

    Returns:
        tuple: (success (bool), leaves (list or str))
    """
    def fetch_leaves(resource_id):
        """
        Recursively fetches leaf nodes of a given resource to build the list of leaves.

        Args:
            resource_id (UUID): The ID of the parent resource to fetch leaf nodes for.

        Returns:
            list: A list of dictionaries, each containing RelationshipID and IsAssessment.
        """
        success, results = list_folder_contents(resource_id)
        if not success:
            return False, results  # Propagate the error message up the call stack

        if isinstance(results, str):  # Handle the "No contents found." message
            return [], None  # Return an empty list for this branch

        leaves = []
        for child in results:
            if child[2] != 'folder':  # Check if the resource type is NOT 'folder'
                leaf_node = {
                    'RelationshipID': child[0],  # RelationshipID
                    'IsAssessment': child[5]     # IsAssessment boolean from query
                }
                leaves.append(leaf_node)
            else:
                # Recursive call to fetch leaves from child folders
                child_success, child_leaves = fetch_leaves(child[0])
                if not child_success:
                    return False, child_leaves  # Propagate the error message up if any
                leaves.extend(child_leaves)  # Append leaves from the recursive call

        return True, leaves

    root_success, root_leaves = fetch_leaves(resource_id)
    if not root_success:
        return False, "Failed to retrieve leaves."

    return True, root_leaves

# Insert a new instance of a resource; the instance is then identified by the relationship_id for parameters and data
# Folders may only be used once, other resources over and over
#
def insert_resource_at_position(parent_resource_id, child_order, child_resource_id, display_name, position='before'):
    """
    Helper function to insert a resource before or after a specified order position within a parent.
    Returns the new RelationshipID on success.
    """
    try:
        with get_db_cursor(commit=False) as cur:
            # Check parent and child types
            cur.execute("SELECT ResourceType FROM Resources WHERE ResourceID = %s", (parent_resource_id,))
            result = cur.fetchone()
            if result is None:
                raise ValueError(f"No resource found with ID {parent_resource_id}.")
            parent_type = result[0]
            if parent_type not in ('folder', 'community_root', 'user_root'):
                raise ValueError(f"Invalid parent resource type {parent_type}.")


            # Fetch the child resource type, name, and used status
            cur.execute("SELECT ResourceType, ResourceName, ResourceUsed FROM Resources WHERE ResourceID = %s", (child_resource_id,))
            result = cur.fetchone()
            if result is None:
                raise ValueError(f"No resource found with ID {child_resource_id}.")
            child_type, child_name, child_used = result
            if child_type in ('community_root', 'user_root'):
                raise ValueError(f"Invalid child resource type {child_type}.")

            # Check if the child is a folder and if it has already been used
            if child_type == 'folder' and child_used:
                raise ValueError(f"The folder {child_name} has already been used and cannot be reused.")

            # Determine the display name
            final_display_name = display_name if display_name else child_name
            
            # Shift existing child orders
            shift_direction = " + 1"
            target_order = child_order + (1 if position == 'after' else 0)
            cur.execute(f"""
                UPDATE ResourceRelationships
                SET ChildOrder = ChildOrder{shift_direction}
                WHERE ParentResourceID = %s AND ChildOrder >= %s
            """, (parent_resource_id, target_order))
            logging.info(f"Child orders updated: shift resources at or above order {target_order}")

            new_order = child_order if position == 'before' else child_order + 1
            # Insert the new relationship and return the new RelationshipID
            cur.execute("""
                INSERT INTO ResourceRelationships (ParentResourceID, ChildResourceID, ChildOrder, DisplayName)
                VALUES (%s, %s, %s, %s) RETURNING RelationshipID
            """, (parent_resource_id, child_resource_id, new_order, final_display_name))
            relationship_id = cur.fetchone()[0]  # Fetch the returned RelationshipID
            logging.info(f"New resource relationship inserted with ID: {relationship_id}")

            # Update the ResourceUsed flag if insert is successful
            cur.execute("""
                UPDATE Resources
                SET ResourceUsed = TRUE
                WHERE ResourceID = %s
            """, (child_resource_id,))
            logging.info(f"ResourceUsed flag set to TRUE for ResourceID: {child_resource_id}")

            # Commit the transaction
            cur.connection.commit()
            logging.info(f"Transaction committed successfully. Resource inserted with RelationshipID: {relationship_id}.")
            return True, relationship_id
    except Exception as e:
        logging.error(f"An error occurred: {e}")
        return False, f"An error occurred: {e}"

def insert_resource_before(parent_resource_id, child_order, child_resource_id, display_name=None):
    """
    Inserts a new child resource before a specified order position within a parent.
    """
    return insert_resource_at_position(parent_resource_id, child_order, child_resource_id, display_name, 'before')

def insert_resource_after(parent_resource_id, child_order, child_resource_id, display_name=None):
    """
    Inserts a new child resource after a specified order position within a parent.
    """
    return insert_resource_at_position(parent_resource_id, child_order, child_resource_id, display_name, 'after')

# This is how a relationship_id can be "removed" from a folder
#
def remove_relationship_from_folder(relationship_id):
    """
    Removes a child resource from its relationship by setting the ChildOrder to NULL.
    This deactivates the child in the context of its relationship without deleting the record.
    """
    update_query = """
        UPDATE ResourceRelationships
        SET ChildOrder = NULL
        WHERE RelationshipID = %s;
    """
    try:
        success, result = execute_db_command(update_query, (relationship_id,))
        if success:
            logging.info(f"Child resource in relationship {relationship_id} has been deactivated.")
            return True, "Child resource successfully deactivated in its relationship."
        else:
            logging.error(f"Failed to deactivate child resource in relationship: {result}")
            return False, result
    except Exception as e:
        logging.error(f"An error occurred while deactivating child resource in relationship: {e}")
        return False, f"An error occurred: {e}"

# We never really delete a relationship_id, this is how it can be restored to the end of a folder
#
def recover_relationship_to_folder_end(relationship_id):
    """
    Recovers a relationship by assigning it a ChildOrder that is one higher than the highest one currently in the folder.
    This is done by automatically retrieving the ParentResourceID from the existing relationship record.
    """
    try:
        # Retrieve the ParentResourceID and the current highest ChildOrder using the given RelationshipID
        get_parent_and_max_order_query = """
            SELECT r.ParentResourceID, COALESCE(MAX(rr.ChildOrder), 0)
            FROM ResourceRelationships r
            LEFT JOIN ResourceRelationships rr ON r.ParentResourceID = rr.ParentResourceID
            WHERE r.RelationshipID = %s
            GROUP BY r.ParentResourceID;
        """
        with get_db_cursor() as cur:
            cur.execute(get_parent_and_max_order_query, (relationship_id,))
            result = cur.fetchone()
            if not result:
                logging.error("No data found for the given RelationshipID.")
                return False, "No data found for the given RelationshipID."

            parent_resource_id, max_child_order = result
            new_child_order = max_child_order + 1

            # Update the ChildOrder for the relationship
            update_order_query = """
                UPDATE ResourceRelationships
                SET ChildOrder = %s
                WHERE RelationshipID = %s;
            """
            cur.execute(update_order_query, (new_child_order, relationship_id))
            cur.connection.commit()
            logging.info(f"Relationship {relationship_id} has been successfully recovered with new ChildOrder {new_child_order}.")
            return True, "Relationship successfully recovered to the folder."

    except Exception as e:
        logging.error(f"An error occurred while recovering the relationship: {e}")
        return False, f"An error occurred: {e}"

# Moving a resource around within and between folders, identified by relationship_id
# This is how the resource instance takes its parameters and data with it
#
def move_relationship_at_position(relationship_id, new_parent_folder, child_order, position='before'):
    """
    Moves a relationship to a new position in the same or a different folder,
    either before or after a specified order position, while ensuring the new parent
    is of the correct type and the child is not a restricted type like 'community_root' or 'user_root'.
    """
    try:
        with get_db_cursor(commit=False) as cur:
            # Check new parent folder type
            cur.execute("SELECT ResourceType FROM Resources WHERE ResourceID = %s", (new_parent_folder,))
            new_parent_type = cur.fetchone()
            if new_parent_type is None:
                raise ValueError(f"No resource found with ID {new_parent_folder}.")
            if new_parent_type[0] not in ('folder', 'community_root', 'user_root'):
                raise ValueError(f"Invalid new parent resource type {new_parent_type[0]}.")

            # Retrieve current details of the relationship and check child type
            cur.execute("""
                SELECT ParentResourceID, ChildOrder, ResourceType
                FROM ResourceRelationships
                JOIN Resources ON ResourceRelationships.ChildResourceID = Resources.ResourceID
                WHERE RelationshipID = %s
            """, (relationship_id,))
            current_details = cur.fetchone()
            if current_details is None:
                raise ValueError(f"No relationship found with ID {relationship_id}.")

            current_parent_id, current_child_order, child_type = current_details
            if child_type in ('community_root', 'user_root'):
                raise ValueError(f"Invalid child resource type {child_type}.")

            # If moving within the same folder and the position is the same, do nothing
            if current_parent_id == new_parent_folder and current_child_order == child_order:
                logging.info("No move necessary as the relationship is already in the correct position.")
                return True, "No move necessary."

            # Update child orders in the target folder to make room
            shift_direction = " + 1"
            target_order = child_order + (1 if position == 'after' else 0)
            cur.execute(f"""
                UPDATE ResourceRelationships
                SET ChildOrder = ChildOrder{shift_direction}
                WHERE ParentResourceID = %s AND ChildOrder >= %s
            """, (new_parent_folder, target_order))
            logging.info(f"Child orders updated: shift resources at or above order {target_order} in new folder.")

            # Update the relationship to move it to the new folder and position
            new_order = child_order if position == 'before' else child_order + 1
            cur.execute("""
                UPDATE ResourceRelationships
                SET ParentResourceID = %s, ChildOrder = %s
                WHERE RelationshipID = %s
            """, (new_parent_folder, new_order, relationship_id))
            logging.info(f"Relationship {relationship_id} moved to new position {new_order} in folder {new_parent_folder}.")

            # Commit the transaction
            cur.connection.commit()
            logging.info("Transaction committed successfully. Relationship moved.")
            return True, "Relationship moved successfully."
    except Exception as e:
        logging.error(f"An error occurred: {e}")
        return False, f"An error occurred: {e}"

def move_relationship_before(relationship_id, new_parent_folder, child_order):
    """
    Moves a relationship to a new position before a specified order within a parent folder.
    """
    return move_relationship_at_position(relationship_id, new_parent_folder, child_order, 'before')

def move_relationship_after(relationship_id, new_parent_folder, child_order):
    """
    Moves a relationship to a new position after a specified order within a parent folder.
    """
    return move_relationship_at_position(relationship_id, new_parent_folder, child_order, 'after')

# This works itself up the folder structure from resource to subsubfolder to subfolder to to community_root
# 
def get_resource_relationship_trail(relationship_id):
    """
    Retrieves an array of RelationshipIDs from the given RelationshipID up to the root resource.

    Args:
        relationship_id (UUID): The ID of the starting resource relationship.

    Returns:
        tuple: (success (bool), message (str or list))
    """
    recursive_query = """
    WITH RECURSIVE RelationshipTrail AS (
        SELECT RelationshipID, ParentResourceID
        FROM ResourceRelationships
        WHERE RelationshipID = %s
    UNION ALL
        SELECT rr.RelationshipID, rr.ParentResourceID
        FROM ResourceRelationships rr
        JOIN RelationshipTrail rt ON rr.ChildResourceID = rt.ParentResourceID
    )
    SELECT RelationshipID FROM RelationshipTrail;
    """
    try:
        with get_db_cursor() as cur:
            cur.execute(recursive_query, (relationship_id,))
            results = cur.fetchall()

            if not results:
                return False, "No relationship found with the given ID"

            relationship_ids = [result[0] for result in results]

            return True, relationship_ids
    except Exception as e:
        logging.error(f"An error occurred while fetching the resource relationship trail: {e}")
        return False, f"An error occurred: {e}"


# ==============================================================================
# Functions having to do with parameters for resource instances relationship_id
# This is a cascading structure, always the more specific values are in effect
# Parameters are stored as JSON
#
 
# This merges the JSON parameter_value to the existing one
#
def set_parameter(community_id, relationship_id, user_id, parameter_value):
    """
    Adds or updates a parameter row in the ResourceParameters table.
    Allows NULL values for RelationshipID and UserID, and updates JSON structure incrementally.

    Args:
        community_id (UUID): The ID of the community.
        relationship_id (UUID, optional): The ID of the relationship, can be None.
        user_id (UUID, optional): The ID of the user, can be None.
        parameter_value (dict): The dictionary containing the parameter values to be set or updated.

    Returns:
        tuple: (success (bool), message (str))
    """
    try:
        with get_db_cursor(commit=True) as cur:
            # Check if the combination already exists
            check_query = """
            SELECT ParameterID, ParameterValues FROM ResourceParameters
            WHERE CommunityID = %s AND (RelationshipID = %s OR RelationshipID IS NULL) AND (UserID = %s OR UserID IS NULL);
            """
            cur.execute(check_query, (community_id, relationship_id, user_id))
            result = cur.fetchone()

            if result:
                parameter_id, existing_params = result
                # Merge existing JSON with new parameter values
                updated_params = json.loads(existing_params) if existing_params else {}
                updated_params.update(parameter_value)  # Merge dictionaries
                # Update existing parameter
                update_query = """
                UPDATE ResourceParameters
                SET ParameterValues = %s
                WHERE ParameterID = %s;
                """
                cur.execute(update_query, (json.dumps(updated_params), parameter_id))
                logging.info(f"Updated parameter for CommunityID: {community_id}, RelationshipID: {relationship_id if relationship_id else 'None'}, UserID: {user_id if user_id else 'None'}")
                return True, "Parameter updated successfully"
            else:
                # Insert new parameter
                insert_query = """
                INSERT INTO ResourceParameters (CommunityID, RelationshipID, UserID, ParameterValues)
                VALUES (%s, %s, %s, %s)
                RETURNING ParameterID;
                """
                cur.execute(insert_query, (community_id, relationship_id, user_id, json.dumps(parameter_value)))
                new_parameter_id = cur.fetchone()[0]
                logging.info(f"Added new parameter with ID: {new_parameter_id} for CommunityID: {community_id}, RelationshipID: {relationship_id if relationship_id else 'None'}, UserID: {user_id if user_id else 'None'}")
                return True, "Parameter added successfully"
    except Exception as e:
        logging.error(f"Failed to set parameter for CommunityID: {community_id}, RelationshipID: {relationship_id if relationship_id else 'None'}, UserID: {user_id if user_id else 'None'}: {e}")
        return False, f"An error occurred: {e}"

# This completely replaces the existing JSON
#
def replace_parameter(community_id, relationship_id, user_id, parameter_value):
    """
    Adds or updates a parameter row in the ResourceParameters table.
    Allows NULL values for RelationshipID and UserID.

    Args:
        community_id (UUID): The ID of the community.
        relationship_id (UUID, optional): The ID of the relationship, can be None.
        user_id (UUID, optional): The ID of the user, can be None.
        parameter_value (JSONB): The JSONB value of the parameter to be set or updated.

    Returns:
        tuple: (success (bool), message (str))
    """
    try:
        with get_db_cursor(commit=True) as cur:
            # Check if the combination already exists
            check_query = """
            SELECT ParameterID FROM ResourceParameters
            WHERE CommunityID = %s AND (RelationshipID = %s OR RelationshipID IS NULL) AND (UserID = %s OR UserID IS NULL);
            """
            cur.execute(check_query, (community_id, relationship_id, user_id))
            existing_id = cur.fetchone()

            if existing_id:
                # Update existing parameter
                update_query = """
                UPDATE ResourceParameters
                SET ParameterValues = %s
                WHERE ParameterID = %s;
                """
                cur.execute(update_query, (parameter_value, existing_id[0]))
                logging.info(f"Updated parameter for CommunityID: {community_id}, RelationshipID: {relationship_id if relationship_id else 'None'}, UserID: {user_id if user_id else 'None'}")
                return True, "Parameter updated successfully"
            else:
                # Insert new parameter
                insert_query = """
                INSERT INTO ResourceParameters (CommunityID, RelationshipID, UserID, ParameterValues)
                VALUES (%s, %s, %s, %s)
                RETURNING ParameterID;
                """
                cur.execute(insert_query, (community_id, relationship_id, user_id, parameter_value))
                new_parameter_id = cur.fetchone()[0]
                logging.info(f"Added new parameter with ID: {new_parameter_id} for CommunityID: {community_id}, RelationshipID: {relationship_id if relationship_id else 'None'}, UserID: {user_id if user_id else 'None'}")
                return True, "Parameter added successfully"
    except Exception as e:
        logging.error(f"Failed to set parameter for CommunityID: {community_id}, RelationshipID: {relationship_id if relationship_id else 'None'}, UserID: {user_id if user_id else 'None'}: {e}")
        return False, f"An error occurred: {e}"

# Careful, this completely deletes the existing JSON
#
def del_parameter(community_id, relationship_id, user_id):
    """
    Deletes a parameter row in the ResourceParameters table based on the provided identifiers.

    Args:
        community_id (UUID): The ID of the community.
        relationship_id (UUID, optional): The ID of the relationship, can be None.
        user_id (UUID, optional): The ID of the user, can be None.

    Returns:
        tuple: (success (bool), message (str))
    """
    conditions = ["CommunityID = %s"]
    params = [community_id]

    if relationship_id is not None:
        conditions.append("RelationshipID = %s")
        params.append(relationship_id)
    else:
        conditions.append("RelationshipID IS NULL")

    if user_id is not None:
        conditions.append("UserID = %s")
        params.append(user_id)
    else:
        conditions.append("UserID IS NULL")

    try:
        with get_db_cursor(commit=True) as cur:
            # Check if the row exists
            check_query = f"SELECT ParameterID FROM ResourceParameters WHERE {' AND '.join(conditions)}"
            cur.execute(check_query, tuple(params))
            if cur.fetchone():
                # Delete the row
                delete_query = f"DELETE FROM ResourceParameters WHERE {' AND '.join(conditions)}"
                cur.execute(delete_query, tuple(params))
                logging.info(f"Deleted parameter row for CommunityID: {community_id}, RelationshipID: {relationship_id if relationship_id else 'None'}, UserID: {user_id if user_id else 'None'}")
                return True, "Parameter deleted successfully"
            else:
                logging.warning(f"No parameter found for CommunityID: {community_id}, RelationshipID: {relationship_id if relationship_id else 'None'}, UserID: {user_id if user_id else 'None'} to delete.")
                return False, "No parameter found to delete"
    except Exception as e:
        logging.error(f"Failed to delete parameter for CommunityID: {community_id}, RelationshipID: {relationship_id if relationship_id else 'None'}, UserID: {user_id if user_id else 'None'}: {e}")
        return False, f"An error occurred: {e}"

# This retrieves the existing JSON
#
def get_parameter(community_id, relationship_id, user_id):
    """
    Retrieves the parameters for a specific community, relationship, and user combination,
    handling None values to match NULL in the database and specific values otherwise.

    Args:
        community_id (UUID or None): ID of the community, None to match NULL.
        relationship_id (UUID or None): ID of the relationship, None to match NULL.
        user_id (UUID or None): ID of the user, None to match NULL.

    Returns:
        tuple: (success (bool), parameters (dict or None))
    """
    # Building the query dynamically to handle None values as SQL NULL and specific values
    conditions = []
    params = []
    if community_id is not None:
        conditions.append("CommunityID = %s")
        params.append(community_id)
    else:
        conditions.append("CommunityID IS NULL")

    if relationship_id is not None:
        conditions.append("RelationshipID = %s")
        params.append(relationship_id)
    else:
        conditions.append("RelationshipID IS NULL")

    if user_id is not None:
        conditions.append("UserID = %s")
        params.append(user_id)
    else:
        conditions.append("UserID IS NULL")

    query = f"""
    SELECT ParameterValues
    FROM ResourceParameters
    WHERE {' AND '.join(conditions)}
    """
    try:
        with get_db_cursor() as cur:
            cur.execute(query, tuple(params))
            result = cur.fetchone()

            if result is not None:
                return True, result[0]  # Return the JSONB stored parameters as a Python dict
            else:
                return True, {}  # Return an empty dictionary if no parameters are found

    except Exception as e:
        logging.error(f"An error occurred while fetching parameters: {e}")
        return False, None

# This assembles the JSON from the complete cascade to get the JSON in effect
#
def get_cascaded_parameters(community_id, relationship_id, user_id):
    """
    Retrieves and merges parameters from the top-level community and relationship down to the specified ones,
    effectively cascading parameters and allowing more specific settings to override general ones.

    Args:
        community_id (UUID): The specific community ID.
        relationship_id (UUID): The specific relationship ID.
        user_id (UUID): The specific user ID.

    Returns:
        tuple: (success (bool), parameters (dict or None))
    """
    try:
        # Start with an empty JSON structure
        cascaded_params = {}

        # Retrieve the community trail
        community_success, community_ids, _ = get_community_relationship_trail(community_id)
        if not community_success:
            return False, None

        # Retrieve the relationship trail
        relationship_success, relationship_ids = get_resource_relationship_trail(relationship_id)
        if not relationship_success:
            return False, None

        # Query for parameters along the community and relationship trail
        parameters_query = """
        SELECT ParameterValues
        FROM ResourceParameters
        WHERE CommunityID = ANY(%s)
          AND RelationshipID = ANY(%s)
          AND UserID = %s
        """
        with get_db_cursor() as cur:
            cur.execute(parameters_query, (community_ids, relationship_ids, user_id))
            results = cur.fetchall()

            # Merge parameters with more specific ones taking precedence
            for result in results:
                # Each result is expected to be a dictionary of parameters
                current_params = result[0]
                # Update the cascaded_params with current_params taking precedence
                cascaded_params = {**cascaded_params, **current_params}

        return True, cascaded_params
    except Exception as e:
        logging.error(f"An error occurred while fetching cascaded parameters: {e}")
        return False, None

