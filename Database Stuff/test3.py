from low_level_database_utils import add_new_user, add_new_authentication_type
from setup_tables import setup_database
import logging

# Set up logging to see detailed output during testing
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

# Initialize the database setup
setup_database()  # Make sure to call the function with ()

# Add a new authentication type and handle the response
success_auth, result_auth = add_new_authentication_type("galaxy")
if success_auth:
    logging.info(f"Authentication type added successfully with ID: {result_auth}")
else:
    logging.error(f"Failed to add authentication type: {result_auth}")

# Add a new user and handle the response
success_user, result_user = add_new_user("zaphod", "galaxy", "Zaphod", "Beeblebrox")
if success_user:
    logging.info(f"User added successfully with ID: {result_user}")
else:
    logging.error(f"Failed to add user: {result_user}")

