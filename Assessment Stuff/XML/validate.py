from lxml import etree
import sys

def validate_xml(xml_path, xsd_path):
    # Load and parse the XML file
    with open(xml_path, 'rb') as xml_file:
        xml_to_check = etree.parse(xml_file)

    # Load and parse the XSD file
    with open(xsd_path, 'rb') as xsd_file:
        xsd_to_check = etree.parse(xsd_file)

    # Get the schema from the XSD document
    xmlschema = etree.XMLSchema(xsd_to_check)

    # Validate the XML against the schema, returning True if it's valid
    is_valid = xmlschema.validate(xml_to_check)

    # Print result of validation and log errors if any
    if is_valid:
        print("Validation successful! XML conforms to the XSD.")
    else:
        print("Validation failed!")
        print(xmlschema.error_log)

# Assuming your XML and XSD files are named 'document.xml' and 'schema.xsd' respectively
validate_xml('problem_structure.xml', 'problem_structure.xsd')

