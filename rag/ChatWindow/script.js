/*
 Project Ethel
 Test HTML client script
 Talks to web socket on port 8000

 Copyright (C) 2024  Gerd Kortemeyer, ETH Zurich

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

messagesContainer = document.getElementById("messages");
messageInput = document.getElementById("message-input");
sendButton = document.getElementById("send-button");

const queryString = window.location.search;
const decodedQueryString = decodeURIComponent(queryString.substring(1));
const queryParams = new URLSearchParams(decodedQueryString);

let port = parseInt(queryParams.get('port'));
if (isNaN(port)) {
   port=8000;
}

chatid='';
regcount=0;

function connectWebSocket() {
    ws = new WebSocket("wss://"+window.location.hostname+":"+port+window.location.search);

    ws.onerror = function(error) {
        console.error("WebSocket error:", error);
    };

    ws.onclose = function() {
        setTimeout(connectWebSocket, 1000); // Try to reconnect after one second
    };

    ws.onmessage = function(event) {
        displayMessage(event.data, false);
    };
}

connectWebSocket(port);

function make_icons_visible() {
  var icons = document.getElementsByClassName("feedback");
  for (var i = 0; i < icons.length; i++) {
     icons[i].style.display = 'inline-block';
  }
}

function make_icons_invisible() {
  var icons = document.getElementsByClassName("feedback");
  for (var i = 0; i < icons.length; i++) {
     icons[i].style.display = 'none';
  }
}

function control(type) {
   switch(type) {
      case 'reference':
         submit('&#128214;');
         break;
      case 'thumbs_up':
         submit('&#x1F44D;');
         break;
      case 'thumbs_down':
         submit('&#x1F44E;');
         break;
    }
}

function escapeHTML(text) {
    if ((text==='&#128214;') ||
        (text==='&#x1F44D;') ||
        (text==='&#x1F44E;')) {
       return text;
    }
    var map = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;',
        '"': '&quot;',
        "'": '&#039;'
    };
    return text.replace(/[&<>"']/g, function(m) { return map[m]; });
}

messageInput.addEventListener("keydown", function(event) {
    if (event.key === "Enter" && !event.shiftKey) {
        event.preventDefault(); // Prevents the default action of the Enter key (new line)
        message = messageInput.value.trim();
        if (message === "") {
            return; // Don't send empty messages
        }
        submit(message);
    }
});

document.getElementById("send-button").addEventListener("click", function() {
    var message = messageInput.value.trim();
    if (message !== "") {
        submit(message);
    }
});

function submit(message) {
    make_icons_invisible();
    if (ws.readyState === WebSocket.OPEN) {
       regcount+=1
       data = JSON.stringify({ "chatid": chatid, "type":"content","repid": "user"+regcount, 
                               "content": escapeHTML(message).replace(/\n/g, '<br>') });
       displayMessage(data, true);
       ws.send(data);
       messageInput.value = "";
       adjustInputHeight();
    } else {
       console.error("WebSocket is not open. ReadyState:", ws.readyState);
    }
} 

function displayMessage(data, isUser) {
    parsedData = JSON.parse(data);
    chatid = parsedData.chatid;
    content = parsedData.content;
    type = parsedData.type;
    repid = parsedData.repid
    if (type === "start" || isUser) {
       messageDiv = document.createElement("div");
       messageDiv.id = repid;
       messageDiv.classList.add("message");
       if (isUser) {
           messageDiv.classList.add("user-message");
       } else {
           messageDiv.classList.add("server-message");
       }
       messagesContainer.appendChild(messageDiv);
       if (!isUser) {
          newSpinner = document.createElement("div");
          newSpinner.classList.add("spinner");
          newSpinner.id="spinner";
          messageDiv.appendChild(newSpinner);
       }
    }
    if (type === "content") {
       document.getElementById(repid).innerHTML+= content;
       messagesContainer.scrollTop = messagesContainer.scrollHeight;
    }
    if (type === "end") {
       MathJax.typeset();
       MathJax.typesetClear();
       const messageElement = document.getElementById(repid);
       messageElement.innerHTML = marked.parse(messageElement.innerHTML);
       messageElement.querySelectorAll('pre code').forEach((block) => {
           hljs.highlightElement(block);
       });
       messageElement.removeChild(spinner);
       make_icons_visible();
    }
}

// Function to adjust the height of the text input
function adjustInputHeight() {
    messageInput.style.height = 'auto'; // Reset height to compute the natural height
    messageInput.style.height = messageInput.scrollHeight + 'px'; // Set the height to the scroll height
}

// Event listener to adjust height on input
messageInput.addEventListener('input', adjustInputHeight);

// Initial adjustment in case there's initial content
adjustInputHeight();
