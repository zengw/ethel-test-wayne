# Project Ethel
# Bootstrap Digester
#
# Copyright (C) 2023  Gerd Kortemeyer, ETH Zurich
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Load library to open Azure LLM connections
#
from openaicalls import make_llm, HistoryBuffer
#
# Langchain stuff
#
from langchain.schema import StrOutputParser
from langchain.prompts import ChatPromptTemplate
from langchain.llms import AzureOpenAI
from langchain.vectorstores import Chroma
from langchain.embeddings import OpenAIEmbeddings
from langchain_core.runnables import RunnableParallel, RunnablePassthrough, RunnableLambda
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain.document_loaders import TextLoader
#
from operator import itemgetter
#
import os
#
import logging
#
# === Main program
# Set up logging
#
logging.basicConfig(filename='../logs/digester.log', level=logging.DEBUG)
logging.info("=============== Starting server")
#
# = Initialize stuff
#
# Get LLM and Embedding
#
model,emb = make_llm()
#
# Open the vectorstore with the documents
#
try:
   vectorstore = Chroma(persist_directory="../database/chroma_db",embedding_function=emb)
   retriever = vectorstore.as_retriever()
except Exception as error:
   logging.error(f"Opening vectorstore: {error}")
   exit()
#
# Read the template file for the input
#
with open('../templates/explain_template.txt', 'r') as file:
    template = file.read()
prompt = ChatPromptTemplate.from_template(template)
#
#
output_parser = StrOutputParser()
#
# Set up the chain
#
setup_and_retrieval = RunnableParallel(
    {"background": itemgetter("segment") | retriever, "previous": itemgetter("previous"), "segment": itemgetter("segment")}
)
chain = setup_and_retrieval | prompt | model | output_parser
#
# Set up text splitter
#
text_splitter = RecursiveCharacterTextSplitter(
    chunk_size=1000, chunk_overlap=200
)
#
# We are done initalizing everything we need
#
try:

    source_dir = '../bootstrap_de'
    target_dir = '../bootstrap_en'
    file_index = 1
    processed_data = ''

    while True:
        file_name = f'chunk{file_index}.txt'
        source_file_path = os.path.join(source_dir, file_name)
        target_file_path = os.path.join(target_dir, file_name)
        print(f"About to work on {source_file_path}")

        # Check if the file exists in the source directory
        if os.path.isfile(source_file_path):
            # Skip processing if file already exists in the target directory
            if not os.path.isfile(target_file_path):
                print(f"Processing {source_file_path}")
                with open(source_file_path, 'r') as source_file:
                    data = source_file.read()
#
# This is where the processing happens
#
                processed_data = chain.invoke({"segment": data, "previous": processed_data})
#
# Store way the file
#
                with open(target_file_path, 'w') as target_file:
                    target_file.write(processed_data)
#
# Process it for embedding
#
                loader = TextLoader(target_file_path)
                docs=loader.load()
                all_splits = text_splitter.split_documents(docs)
                ids = [f"id_{file_index}_{i+1}" for i in range(len(all_splits))]
                Chroma.from_documents(all_splits,emb,ids=ids,persist_directory="../database/chroma_db")
#
# Done processing
#                
                with open(target_file_path, 'w') as target_file:
                    target_file.write(processed_data)
        else:
            # Stop the loop if the file doesn't exist in the source directory
            print("Done.")
            exit()

        file_index += 1
except Exception as error:
   logging.error(f"Processing: {error}")
   exit()
