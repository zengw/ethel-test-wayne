#!/bin/bash

# Define the base path for volumes
base="/Users/korte/Documents/ETHel/langchain/docker/dockermounts"

# Function to check if the input is a number
is_number() {
    if ! [[ $1 =~ ^[0-9]+$ ]]; then
        return 1 # False
    else
        return 0 # True
    fi
}

# Get the port and name from the command line arguments
port="$1"
name="$2"

# Check if the name argument is provided
if [ -z "$name" ]; then
    echo "No name argument provided."
    exit 1
fi

# Check if databases/$name exists under the base path
if [ ! -d "$base/databases/$name" ]; then
    echo "The database does not exist."
    exit 1
fi

if ! is_number "$port"; then
    echo "Error: Port number must be numeric."
    exit 1
fi

if lsof -i tcp:${port} &> /dev/null; then
    echo "Port ${port} is already in use."
    exit 1
fi

# Run the Docker container
 docker run -d \
    -p $port:8000 \
    -v "$base/app:/app" \
    -v "$base/logs:/logs" \
    -v "$base/templates:/templates" \
    -v "$base/configs:/configs" \
    -v "$base/databases/$name:/database" \
    ethel_rag_server
