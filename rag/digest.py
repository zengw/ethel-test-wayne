from openaicalls_cmd import make_llm
from langchain.schema import StrOutputParser
from langchain.prompts import ChatPromptTemplate
from langchain.llms import AzureOpenAI

from langchain.vectorstores import Chroma
from langchain.embeddings import OpenAIEmbeddings
from langchain_core.runnables import RunnableParallel, RunnablePassthrough, RunnableLambda

from langchain.document_loaders import TextLoader
from langchain.text_splitter import RecursiveCharacterTextSplitter

import time

model,emb = make_llm()


loader = TextLoader("./docs/main.tex")
docs=loader.load()

text_splitter = RecursiveCharacterTextSplitter(
    chunk_size=1000, chunk_overlap=200, add_start_index=True
)
all_splits = text_splitter.split_documents(docs)

# Set the batch size and delay time
batch_size = 50  # Adjust this based on your rate limit
delay_time = 5  # Adjust the delay time in seconds

# Function to process documents in batches
def process_in_batches(documents, batch_size, delay_time):
    for i in range(0, len(documents), batch_size):
        batch = documents[i:i + batch_size]
        ids = [str(j) for j in range(i, i+batch_size)]
        try:
           Chroma.from_documents(batch,emb,ids=ids,persist_directory="./chroma_db")
        except Exception as error:
           print(f"Handling request: {error}")
           exit()
        print(i)
        time.sleep(delay_time)  # Pause between batches

# Call the batch processing function
process_in_batches(all_splits, batch_size, delay_time)
print("Done.")
